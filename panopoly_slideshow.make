; Panopoly Slideshow Makefile

api = 2
core = 7.x

; Flex Slider module

projects[flexslider][subdir] = contrib

; Flex Slider library

libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/zipball/master"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"
